"use strict";

const GET_INFO = "http://ip-api.com/json/?fields=61439";

class Button {
  constructor(button) {
    this.button = button;
  }
  clickCheck() {
    this.button.addEventListener("click", getData);
  }
}

const button = new Button(document.getElementById("ip_button"));
button.clickCheck();

async function getData() {
  if (this.className === "clicked") return;
  this.classList.add("clicked");
  const request = await fetch(GET_INFO);
  const data = await request.json();
  const p = document.createElement("p");
  p.textContent = `Country: ${data.country}, city: ${data.city}, region: ${data.regionName}, provider: ${data.isp}`;
  document.body.append(p);
}
